---
title: subl 🐹
subtitle : A sophisticated text editor for code, markup and prose.
category: Программы
position: 4
menuTitle: Sublime Text 3
packages:
  - Package Control
  - A File Icon
  - AdvancedNewFile
  - AutoFileName
  - Babel
  - Bracket Highlighter
  - Case Conversion
  - Color Highlighter
  - ColorHelper
  - Console Wrap
  - DocBlockr
  - Dockerfile Syntax Highlighting
  - DotENV
  - EditorConfig
  - Emmet
  - Gist
  - Handlebars
  - ImagePreview
  - JSON Key-Value
  - Laravel Blade Highlighter
  - nginx
  - Pug
  - Sass
  - Stylus
  - SublimeLinter
  - Sublime Linter-eslint
  - Tailwind CSS Autocomplete
  - TypeScript
  - Vue Syntax Highlight
sublLintPackages:
  - SublimeLinter
  - SublimeLinter-tslint
  - SublimeLinter-eslint
---
[![my-subl](https://mioe.gitlab.io/mi-notes/screenshots/my-subl.png)](https://mioe.gitlab.io/mi-notes/screenshots/my-subl.png)

## Hotkeys
```bash
ctrl+P # открыть файл в текущей директории
ctrl+shift+P # выполнить действие
ctrl+K > ctrl+B # показать панель файлов/скрыть
ctrl+{ # аналог shift + tap
ctrl+} # tab
```

## Hotkeys Plus
<code-group>
  <code-block label="Linux" active>

```bash
super+alt+N # AdvancedNewFile (дополнительный пакет)
```
    
  </code-block>
</code-group>

## Пакеты
<list :items="packages"></list>

## Sublime Linter
> SublimeLinter is a plugin for Sublime Text that provides a framework for linting code.
> http://www.sublimelinter.com/en/stable/

![Sublime Linter](https://www.sublimelinter.com/en/stable/_images/screenshot.png)

<list :items="sublLintPackages"></list>

## Кастомный шрифт
> https://www.jetbrains.com/ru-ru/lp/mono/

## Настройка
```json[Preferences.sublime-settings]
{
  "draw_white_space": "all",
  "font_face": "JetBrains Mono",
  "font_size": 11,
  "ignored_packages":
  [
    "Vintage"
  ],
  "show_encoding": true,
  "tab_size": 2,
  "theme": "Adaptive.sublime-theme",
  "translate_tabs_to_spaces": true
}
```

## Snippets
> https://github.com/mioe/sublime-snippets

Просто клонируй в `папку настроек` саблайма:
```bash
cd ~/.config/sublime-text-3/Packages/User/
git clone git@github.com:mioe/sublime-snippets.git
```
