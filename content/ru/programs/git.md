---
title: git
subtitle :  Распределённая система управления версиями.
category: Программы
position: 2
menuTitle: Git
---
## Первоначальная настройка Git
```bash
git config --global user.name NAME
```
```bash
git config --global user.email EMAIL@example.com
```

## Генерация открытого SSH ключа
```bash
ssh-keygen
```
```bash
cat ~/.ssh/id_rsa.pub
```

## Удаление файла из истории гита
```bash
git rm --cached package-lock.json
```

## Замена remote branch
```bash
git remote -v # отобразить удаленные пути
# output:
# origin  https://gitlab.com/repo/OLD.git (fetch)
# origin  https://gitlab.com/repo/OLD.git (push)
git remote rm origin # удалить ссылку на удаленный репозиторий 
git remote -v
# output: (empty)
git remote add origin git@gitlab.com:repo/NEW.git     # добавить новый удаленный путь (ssh)
git remote add origin https://gitlab.com/repo/NEW.git # (html)
git remote -v
# output:
# origin  https://gitlab.com/repo/NEW.git (fetch)
# origin  https://gitlab.com/repo/NEW.git (push)
```
