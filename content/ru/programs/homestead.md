---
title: homestead
category: Программы
position: 3
menuTitle: Homestead
---
## Commands
```bash
vagrant up # запуск Homestead
vagrant up --provision # запустить с новыми изменениями
vagrant reload --provision # перезапустить с новыми изменениями
vagrant halt # выключить виртуалку
```

## Ошибки внутри виртуалки
### [RuntimeException]
При запуске команды `composer install` выдается ошибка (наблюдается на `symfony`):

<alert type="danger">

[RuntimeException]

Could not delete `/home/vagrant/code/projectSymfony/vendor/composer/package-versions-deprecated/src/PackageVersions`

</alert>

<alert type="success">

Лечение: `composer install --no-plugins`

</alert>


### symlink(): Protocol error
При выполнении команды `php artisan storage:link` выдается ошибка (наблюдается на винде):

<alert type="danger">

ErrorException  : `symlink(): Protocol error`
```
  at `/home/vagrant/code/projectLaravel/vendor/laravel/framework/src/Illuminate/Filesystem/Filesystem.php:263`
    259|      */
    260|     public function link($target, $link)
    261|     {
    262|         if (! windows_os()) {
  > 263|             return symlink($target, $link);
    264|         }
    265|
    266|         $mode = $this->isDirectory($target) ? 'J' : 'H';
    267|

  Exception trace:
  1   symlink()
      /home/vagrant/code/projectLaravel/vendor/laravel/framework/src/Illuminate/Filesystem/Filesystem.php:263

  2   Illuminate\Filesystem\Filesystem::link()
      /home/vagrant/code/projectLaravel/vendor/laravel/framework/src/Illuminate/Foundation/Console/StorageLinkCommand.php:35
  Please use the argument -v to see more details.
```

</alert>

<alert type="success">

Лечение:

1. Выключить Homestead `vagrant halt`
2. Запустить `cmd` через администратора
3. Включить Homestead и повторно ввести команду

</alert>

