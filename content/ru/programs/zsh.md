---
title: zsh
category: Программы
position: 14
menuTitle: zsh
---

Z shell, **zsh** — одна из современных командных оболочек UNIX, использующаяся непосредственно как интерактивная оболочка, либо как скриптовый интерпретатор. Zsh является расширенным bourne shell с большим количеством улучшений.

## Установка
Смена `bash` терминала на [zsh](https://gist.github.com/tsabat/1498393)
```bash
# for arch/manjaro
sudo pacman -S zsh
# for ubuntu
sudo apt-get install zsh

wget https://github.com/robbyrussell/oh-my-zsh/raw/master/tools/install.sh -O - | zsh
chsh -s `which zsh`
sudo shutdown -r 0
```
Добавляем тему для `zsh` - [Spaceship ZSH](https://github.com/denysdovhan/spaceship-prompt)
```bash
git clone https://github.com/denysdovhan/spaceship-prompt.git "$ZSH_CUSTOM/themes/spaceship-prompt" --depth=1
ln -s "$ZSH_CUSTOM/themes/spaceship-prompt/spaceship.zsh-theme" "$ZSH_CUSTOM/themes/spaceship.zsh-theme"
```
и изменяем дефолтную тему `.zshrc`, и добавляем надстройку темы:
```bash
ZSH_THEME="spaceship"
...
# Spaceship ZSH - https://github.com/denysdovhan/spaceship-prompt
SPACESHIP_PROMPT_ADD_NEWLINE="false"
```
применяем изменения:
```bash
source ~/.zshrc
```

<alert>

**Если в терминале не отображается иконки**

Нужно установить [Powerline Fonts](https://github.com/powerline/fonts):
```bash
# for arch/manjaro
sudo pacman -S powerline powerline-fonts
# for ubuntu
sudo apt-get install fonts-powerline
```

</alert>

<alert>

**Для WSL нужно установить шрифты**

Запустите `PowerShell` с правами администратора и выполнить:
```powershell
git clone https://github.com/powerline/fonts.git --depth=1
cd fonts
Set-ExecutionPolicy Bypass
./install.ps1
cd ..
Remove-Item fonts -Recurse -Force
```

</alert>

<alert>

**Powerline Font установка на MacOS**

```bash
git clone https://github.com/powerline/fonts.git
cd fonts
./install.sh
```

</alert>

## Дополнительные плагины
Установка плагинов для `zsh`:
```bash
# zsh-completions
git clone https://github.com/zsh-users/zsh-completions ${ZSH_CUSTOM:=~/.oh-my-zsh/custom}/plugins/zsh-completions
# zsh-autosuggestions
git clone https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions
```
далее отредактировать `~/.zshrc`:
```bash
...
plugins=(
    command-not-found
    docker
    git
    golang
    npm
    sudo
    systemd
    yarn
    zsh-autosuggestions
    zsh-completions
)
```
применяем изменения:
```bash
source ~/.zshrc
```
