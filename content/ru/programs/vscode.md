---
title: code
subtitle : vscode - текстовый редактор от компании Microsoft, написан на электроне.
category: Программы
position: 5
menuTitle: VS Code
extensions:
  - Bracket Pair Colorizer 2
  - change-case
  - Color Manager
  - CSV to Table
  - Docker
  - DotENV
  - EditorConfig for VS Code
  - ESLint
  - Gist
  - GitHub Theme
  - html2pug
  - Laravel Blade
  - Live Share
  - Material Icon Theme
  - PHP Intelephense (Ben Newburn)
  - Sass
  - Sublime Text Keymap and Settings Importer
  - Tailwind CSS IntelliSense
  - The Pattern Language (TPL)
  - Twig
  - Vetur (hikerpig) v0.24.1
  - Visual Studio IntelliCode
  - Vscode Google Translate
  - WindiCSS IntelliSense
  - XML Tools
  - YAML
  - Vue (for Vue3)
  - Vetur (for Vue3)
  - Volar (for Vue3+TS)
---
[![my-code](https://mioe.gitlab.io/mi-notes/screenshots/my-code.png)](https://mioe.gitlab.io/mi-notes/screenshots/my-code.png)

## Hotkeys
```bash
crtl+K > V # превью Markdorw файлов
shift + alt + F # форматирование кода в читаемый вид
```

## Запуск линтера внутри подпапки
Допустим такая ситуация:
```back
project/
  backend/
    ..
    .eslintrc.js
  frontend/
    ..
    .eslintrc.js
  ..
```
Чтобы заставить каждый линтер работать в своей папке создаем `.vscode/settings.json`:
```json
{
  "eslint.workingDirectories": [
    "./backend",
    "./frontend",
  ]
}
```

## Расширения
<list :items="extensions"></list>

## Кастомный шрифт
> https://www.jetbrains.com/ru-ru/lp/mono/

## Настройка
```json[settings.json]
{{
    "git.ignoreMissingGitWarning": true,
    "workbench.startupEditor": "newUntitledFile",
    "workbench.iconTheme": "material-icon-theme",
    "diffEditor.ignoreTrimWhitespace": true,
    "editor.renderWhitespace": "all",
    "editor.tabSize": 2,
    "editor.formatOnPaste": true,
    "editor.multiCursorModifier": "ctrlCmd",
    "editor.fontSize": 16,
    "editor.wordWrap": "on",
    "editor.fontFamily": "'JetBrains Mono'",
    "editor.fontLigatures": true,
    "editor.snippetSuggestions": "top",
    "extensions.ignoreRecommendations": true,
    "javascript.suggest.autoImports": false,
    "material-icon-theme.folders.theme": "classic",
    "editor.suggestSelection": "first",
    "vsintellicode.modify.editor.suggestSelection": "automaticallyOverrodeDefaultValue",
    "terminal.integrated.shell.linux": "/bin/zsh",
    "terminal.integrated.shell.windows": "C:\\Windows\\System32\\cmd.exe",
    "git.confirmSync": false,
    "explorer.confirmDragAndDrop": false,
    "explorer.confirmDelete": false,
    "workbench.editor.wrapTabs": true,
    "workbench.colorTheme": "GitHub Dark",
    "windicss.enableCodeFolding": false
}
```
