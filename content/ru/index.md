---
title: mi-notes
subtitle : Мои заметки обо всем.. 📁 📌 📗 🎉 🐧 💻
category: Заметки
position: 1
menuTitle: Привет мир!
---
by [mioe](https://github.com/mioe) && [annablgkv](https://github.com/annablgkv)

<p class="flex items-center">Попробуй переключиться между светлым и темным режимами:&nbsp;<app-color-switcher class="inline-flex ml-2"></app-color-switcher></p>

[![logo](https://mioe.gitlab.io/mi-notes/images/docs-logo-v2.jpg)](https://mioe.gitlab.io/mi-notes/images/docs-logo-v2.jpg)
