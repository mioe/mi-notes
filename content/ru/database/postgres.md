---
title: postgres
subtitle : Свободная объектно-реляционная система управления базами данных.
category: Database
position: 7
menuTitle: PostgreSQL
---
## Commands
```bash
psql -h localhost -U homestead # вход в postgres
```

```sql
CREATE DATABASE "db-name"; 
```
