---
title: hosts
category: Заметки
position: 6
menuTitle: Hosts File
---
## Where is the Hosts File Located?
```bash
subl C:\Windows\System32\drivers\etc\hosts # windows 10
subl /etc/hosts # linux
subl /private/etc/hosts # macos
```
