---
title: node
category: Hello World
position: 10
menuTitle: Node.js
---

## Просмотр установленных пакетов
```bash
# yarn
yarn list
yarn global list # вывод глобальнных
# npm
npm list --depth=0
npm list -g --depth=0
```

## Установка `nvm` - Node Version Manager
Ссылка [на установку nvm - Nodejs Version Manager](https://github.com/nvm-sh/nvm).

далее добавляем в `~/.zshrc`:
```bash
...
# set global path for nvm
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion
```

Установка `nodejs` с помощью `nvm`:
```bash
nvm install node
nvm use node
nvm alias default 14 # set default node version
```

## Установка `yarn` - пакетный менеджер
```bash
npm i -g yarn
```
