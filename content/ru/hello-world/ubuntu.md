---
title: ubuntu 
category: Hello World
position: 12
menuTitle: Ubuntu 
---

## cmd
```bash
lsb_release -a # номер версии ubuntu
dpkg-query -l # просмотр установленных пакетов
```

## Установка
Все как обычно, выбираем язык ос `English`

## Первый шаг
Установка гита и курла:
```bash
sudo apt-get install git curl
```
Настойка гита, инициализация глобального пользователя:
```bash
git config --global user.name nickname
git config --global user.email example@gmail.com
```
Далее генерация `ssh-ключа`:
```bash
ssh-keygen
cat ~/.ssh/id_rsa.pub 
```
Смена `bash` терминала на [zsh](https://gist.github.com/tsabat/1498393).<br>
Процесс установки - [тут](/programs/zsh).

Фикс русского времени и добавления русской раскладки:

<alert>
ubuntu-gnome(20.04)

`Setting` -> `Region & Language`<br>
|<span class="ml-4">`Formats` -> `United Kingdom`</span><br>
|<span class="ml-4">`Input Sources` -> `+` -> `Russian` -> `Add`</span>

</alert>

<alert>
xubuntu(20.04)

`Settings Manager`<br>
|<span class="ml-4">`Language Support` -> `Regional Formats` -> `English (United Kingdom)`<br>
|<span class="ml-4">`Keyboard` -> `Layout` -> Keyboard layout `Add` -> `Russian`<br>
|<span class="ml-4">`Keyboard` -> `Layout` -> `Change layout option` -> `Win+Space`

</alert>

## Установка программ
По нужной мелочи:
```bash
sudo apt-get install keepass2
```

```bash
sudo apt-get install neofetch flameshot htop 
```

```bash
sudo apt-get install caffeine
```
Настойка `caffeine` (добавляем в автозапуск):

<alert>

ubuntu-gnome (20.04):

`Startup Applications Preferences` -> `Caffeine` -> `Edit` -> command: `caffeine-indicator`

</alert>

<alert>

xubuntu (20.04):

`Settings Manager` -> `Session and Startup` -> `Application Autostart` -> `+ Add`<br>
|<span class="ml-4">name: `autorun caffeine`, desctiption: `caffeine`, command: `caffeine-indicator`, trigger: `on login` 

</alert>


Настойка `flameshot`:

<alert type="warning">

После запуска заходим в настройки (клик на иконку -> `Configuration`)

General -> `ON` Launch at startup

</alert>

Добавляем хоткей для быстрых скриншотов `flameshot`:

<alert>

ubuntu-gnome (20.04):

`Setting` -> `Keyboard Shortcuts` -> `+` внизу списка<br>
|<span class="ml-4">name: `flameshot`, command: `flameshot gui`, shortcut: `Shift+Super+S`

</alert>

<alert>

xubuntu (20.04):

`Settings Manager` -> `Keyboard` -> `Application Shortcuts` -> `+ Add`<br>
|<span class="ml-4">command: `flameshot gui`, shortcut: `Shift+Super+S`

</alert>


Telegram, качаем линукс версию телеграмма - [telegram.org](https://telegram.org)
```bash
cd ~/Downloads # переход в папку загрузок
tar -xf tsetup.2.3.2.tar.xz  # разпоковка телеграмма
mkdir ~/.programs # создание папки для программ (если уже есть пропустить)
mv Telegram ~/.programs # перенос папки телеграмма в папку с программами 
cd ~/.programs/Telegram
./Telegram # первый запуск телеграмма
```
[Spotify](https://www.spotify.com/ru-ru/download/linux/) - музыка
 - `Install via command line` -> `Debian / Ubuntu`

[OBS](https://obsproject.com/wiki/install-instructions#linux) - Open Broadcaster Software
```bash
sudo apt install ffmpeg
sudo add-apt-repository ppa:obsproject/obs-studio
sudo apt update
sudo apt install obs-studio
```

Настойка `obs-studio`:

<alert>

`Setting` -> `Output` -> `Recording` -> `Recording Format` -> `mp4`<br>
`Video` -> `Output (Scaled) Resolution` -> `1920x1080`<br>
`Common FPS Values` -> `60`<br>

</alert>


## Установка рабочих программ
[Sublime Text && Merge](https://www.sublimetext.com/docs/3/linux_repositories.html) - настройка [subl](/programs/subl)
``` bash
wget -qO - https://download.sublimetext.com/sublimehq-pub.gpg | sudo apt-key add -
sudo apt-get install apt-transport-https
echo "deb https://download.sublimetext.com/ apt/stable/" | sudo tee /etc/apt/sources.list.d/sublime-text.list
sudo apt-get update
sudo apt-get install sublime-text sublime-merge
```

[code.visualstudio.com](https://code.visualstudio.com/) - настройка [vscode](/programs/vscode) (основной редактор)

[nvm - Nodejs Version Manager](https://github.com/nvm-sh/nvm) - настройка [nvm](/hello-world/node)

[DBeaver](https://dbeaver.io/download/) - клиент для баз данных

[MongoDB Compass](https://www.mongodb.com/try/download/compass) - клиент для mongo

[docs.docker.com](https://docs.docker.com/engine/install/ubuntu/#install-using-the-repository) - заметки [docker](/hello-world/docker)
```bash
sudo apt-get update

sudo apt-get install \
  apt-transport-https \
  ca-certificates \
  curl \
  gnupg-agent \
  software-properties-common

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
# output OK

sudo apt-key fingerprint 0EBFCD88
# проверка подлинности смотри что должно выдать в офф доке вверху ссылка
```
Если все ОК и подлинность совпала, то можно установить сам докер:
```bash
sudo add-apt-repository \
  "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) \
  stable"

sudo apt-get update
sudo apt-get install docker-ce docker-ce-cli containerd.io
```
Установка `docker-compose`:
```yarn
sudo apt-get install docker-compose
```
Настройка группы `docker`, для использования докер без `sudo`:
```bash
sudo usermod -aG docker ${USER}
sudo shutdown -r 0
```

## Внешний вид
### anna@winni
Включить черную тему:
 - `Setting` -> `Appearance` -> `Dark`
Добавление [gnome extensions](https://extensions.gnome.org/):
```yarn
sudo add-apt-repository universe
sudo apt-get install gnome-tweak-tool
sudo apt-get install gnome-shell-extensions
sudo apt-get install chrome-gnome-shell
```
Install [Firefox Add-on](https://addons.mozilla.org/en-US/firefox/addon/gnome-shell-integration/)

Теперь можно добавлять новые расширения:
[dash-to-panel](https://extensions.gnome.org/extension/1160/dash-to-panel/) - панель как у винды
Настройка `dash-to-panel`:
 - `Tweaks` -> `Extensions` -> `Dash to panel` -> `Edit`
   - `Styles` -> Panel Size: `48px`, App Icon Margin: `0px`, App Icon Padding: `8px`
   - `Fine-Tune` -> Tray Item Padding: `8px`, Status Icon Padding: `2px`, LeftBox Padding: `-1px`
   
[Clipboard Indicator](https://extensions.gnome.org/extension/779/clipboard-indicator/) - запоминалка буфера обмена
