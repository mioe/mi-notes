---
title: manjaro
category: Hello World
position: 15
menuTitle: Manjaro
---
Manjaro Linux или **Manjaro** — дистрибутив GNU/Linux, основанный на **Arch Linux**, использующий модель обновлений rolling release. Официально доступно несколько версий: с рабочим окружением Xfce, KDE или GNOME.

## Ссылки
- [wiki.archlinux.org](https://wiki.archlinux.org/) - документации Arch Linux и архив пакетов/AUR.
- [wiki.manjaro.org](https://wiki.manjaro.org) - архив стабильных пакетов для Manjaro.

[![my-manjaro](https://mioe.gitlab.io/mi-notes/screenshots/2021-01-16_22-53.png)](https://mioe.gitlab.io/mi-notes/screenshots/2021-01-16_22-53.png)

