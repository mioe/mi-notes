const { CI_PAGES_URL } = process.env
const base = CI_PAGES_URL && new URL(CI_PAGES_URL).pathname

import theme from "@nuxt/content-theme-docs"

export default theme({
  /*
   ** You can extend the nuxt configuration
   ** Doc: https://content.nuxtjs.org/themes-docs#nuxtconfigjs
   */
  i18n: {
    locales: () => [
      {
        code: 'ru',
        iso: 'ru-RU',
        file: 'ru-RU.js',
        name: 'Русский'
      },
      // {
      //   code: 'en',
      //   iso: 'en-US',
      //   file: 'en-US.js',
      //   name: 'English'
      // },
    ],
    defaultLocale: 'ru'
  },

  /*
   ** Customize the generated output folder
   */
  generate: {
    dir: 'public',
  },

  /*
   ** Customize the base url
   */
  router: {
    base,
  },

  server: {
    port: 3333, // default: 3000
    host: 'localhost' // default: localhost
  },

  loading: {
    color: '#003e27',
  },
})
